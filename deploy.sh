#!/usr/bin/env bash
set -e

read -rp "Agregar clave de admin para mysql: " rootPassword
[ -z "$rootPassword" ] && echo "La clave es requerida!!" && exit 1

echo 'Se creara el secret database-credentials en el cluster'
echo ' '
kubectl create secret generic database-credentials --from-literal=DB_PASS="$rootPassword"

echo 'Se comenzara el deploy de los configmaps'
echo ' '
kubectl apply -f manifests/configmap.yaml

echo 'Se comenzara el deploy de la base de datos'
echo ' '
kubectl apply -f manifests/sts-db.yaml
kubectl apply -f manifests/service-db.yaml

sleep 120

echo 'Se creara la base de datos de simpsons_quotes'
echo ' '
START=0
FINISH=$(kubectl get sts/simpsons-quote-db -o=jsonpath="{.spec.replicas}")
while [[ $START < $FINISH ]]
do
    echo "Aplicando en el $START statefulset"
    kubectl exec po/simpsons-quote-db-$START -- /bin/sh /configuration/script.sh && \
    echo ' '
    echo 'Finalizado'
    START=$(($START+1))
done


echo 'Se comenzara el deploy de la api'
echo ' '
kubectl apply -f manifests/deployment-api.yaml
kubectl apply -f manifests/service-api.yaml

echo 'Exponiendo el servicio'
echo ' '
kubectl apply -f manifests/ingress.yaml

sleep 30
echo ''
echo "El servicio se encuentra expuesto en la siguiente url: http://$(kubectl get ingress/simpsons-quote-api -o=jsonpath='{.status.loadBalancer.ingress[].ip}')/docs"