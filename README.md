# simpsons-quotes-api

## Prerequisitos
Dentro de la carpeta **manifests** se encuentran los manifiestos de kubernetes a aplicar.
Para que se puedan ejecutar todos, es necesario tener deployado en el cluster un ingress.
En esta implementacion se uso el ingress nginx, el cual [AQUI ESTA EL INSTRUCTIVO](https://kubernetes.github.io/ingress-nginx/deploy/#quick-start)
>NOTA: El ingressClassName que se aplico en este ejercicion es el que viene por default

## Como deployar la aplicacion?
Se comparten de formas:
1. Primero hacemos el clonado del repositorio
```bash
git clone https://gitlab.com/jeyseven/redbee-challenge.git
```
2. Nos movemos al directorio
```bash
cd redbee-challenge
```
3. Tenemos dos formas de hacer el deploy:
    * Usando el script **deploy.sh**
        1. Simplemente ejecutamos **deploy.sh** y nos va a pedir la password para poner en el secret de configuracion de base
    * Aplicando manualmente cada manifiesto
        1. Hay crear el secret **database-credentials**
        ```bash
        kubectl create secret generic database-credentials --from-literal=DB_PASS=<ACA VA EL PASSWORD DE MYSQL>
        ``` 
        2. Aplicamos los configmap
        ```bash
        kubectl apply -f manifests/configmap.yaml
        ```
        3. Aplicamos los recursos de la base de datos
        ```bash
        kubectl apply -f manifests/sts-db.yaml
        kubectl apply -f manifests/service-db.yaml
        ```
        4. Una vez la base de datos este arriba, ejecutamos un exec en cada replica del Statefulset que se haya solicitado.
        ```bash
        ### Ejemplo primer pod del sts
        kubectl exec po/simpsons-quote-db-0 -- /bin/sh /configuration/script.sh
        ```
        5. Deployamos los recursos de la api
        ```bash
        kubectl apply -f manifests/deployment-api.yaml
        kubectl apply -f manifests/service-api.yaml
        ```
        6. Exponemos el servicio
        ```bash
        kubectl apply -f manifests/ingress.yaml
        ```
        7. Para conocer la IP podemos del Ingres podemos ejecutar lo siguiente
        ```bash
        kubectl get ingress/simpsons-quote-api -o=jsonpath='{.status.loadBalancer.ingress[].ip}'
        ```